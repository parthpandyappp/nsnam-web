---
title: Bugzilla
layout: page
permalink: /develop/tools/bugzilla/
---
The canonical ns-3 bug tracker is based on bugzilla. The server is located [there](/bugzilla). Instructions on how to report bugs are detailed [elsewhere](/support/report-a-bug). The following text describes and gives hints on how developers and maintainers use the bug tracker. If you have any question that is not answered here, please, send an email to the [ns-developers mailing-list](/developers/tools/mailing-lists#ns-developers).

# Bug Priorities

Bugzilla provides a very flexible mechanism to manage bugs, allowing users and maintainers to assign both priorities (P1 to P5 where P1 is highest priority) and severities (ranging from Blocker to Enhancement). While some maintainers do carefully manage both the priority and the severity of each bug, we use in general only the priority field to manage each release.

The highest priorities are assigned by the maintainers to identify critical path issues for upcoming releases. In every release cycle, the maintainers evaluate the list of bugs and promote certain of these bugs to P1 or P2. This places them directly on the critical path for a given release, and they must be fixed before that release can go forward.

  * P1: The most severe bugs that will block the upcoming release of ns-3 if they are not fixed.
  * P2: Very serious bugs that, for some reason, cannot be fixed in the upcoming release.
  * P3, P4, P5: everything else.

In general, if a bug is an error in the implementation or could conceivably cause erroneous simulation results for unsuspecting users, it should be labeled at least P2.
