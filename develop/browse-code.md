---
layout: page
title: Browse code
permalink: /develop/browse-code/
---
The ns-3 project uses the [GitLab.com](https://about.gitlab.com) cloud service to host various Git repositories that contain the ns-3 source code. The main development tree can be easily [browsed online](https://gitlab.com/nsnam/ns-3-dev).

Older Mercurial repositories from the project (2018 and earlier) can be [browsed here](http://code.nsnam.org/).
