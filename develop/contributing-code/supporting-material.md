---
title: Supporting material
layout: page
permalink: /develop/contributing-code/supporting-material/
---
Often we have observed that contributors will provide nice new classes implementing a new model, but will fail to include supporting test code, example scripts, and documentation. Therefore, we ask people submitting the code (who are in the best position to do this type of work) to provide documentation, test code, and example scripts.

Note that you may want to go through multiple phases of code reviews, and all of this supporting material may not be needed at the first stage (e.g. when you want some feedback on public API header declarations only, before delving into implementation). However, when it times come to merge your code, you should be prepared to provide these things, as fits your contribution (maintainers will provide some guidance here).

# Including examples

For many submissions, it will be important to include at least one example that exercises the new code, so the reviewer understands how it is intended to be used. For final submission, please consider to add as many examples as you can that will help new users of your code. The <tt>samples/</tt> and <tt>examples/</tt> directories are places for these files.

# Testing

Every new code should include automated tests. These should, where appropriate, be unit tests of model correctness, validation tests of stochastic behavior, and overall system tests if the model's interaction with other components must be tested. The test code should try to cover as much model code as possible, and should be checked as much as possible by regression tests.

The ns-3 manual provides documentation and suggestions for how to write tests.

# Documentation

If you add a new features, or make changes to existing features, you need to update existing or write new documentation and example code. Consider the following checklist:

  * doxygen should be added to header files for public classes and methods, and should be checked for doxygen errors
  * incompatible API changes must be documented in <tt>CHANGES.html</tt>
  * new features should be described in <tt>RELEASE_NOTES</tt>
  * new API or changes to existing API must update the inline doxygen documentation in header files
  * consider updating or adding a new section to the tutorial in <tt>doc/tutorial/</tt>
  * consider updating or adding a new section to the manual in <tt>doc/manual/</tt>
  * update the <tt>AUTHORS</tt> file as appropriate
