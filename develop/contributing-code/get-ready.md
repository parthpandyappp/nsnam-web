---
title: Get ready
layout: page
permalink: /develop/contributing-code/get-ready/
---
# Description

For each code submission, include a description of what your code is doing, and why. Ideally, you should be able to provide a summary description in a 5-line paragraph with a 1-line (15 word) subject. If you fix a bug filed in our [bugzilla database](/bugzilla), the subject should include first the bug number, and then, the bug title. For example: <tt>bug 558: qos-tag.h is gone missing from wscript</tt>

Bonus points go to submitters who provide a description of their testing strategy.

# Minimize the size of each submission

Each submission should be as small as possible. Ideally, each submission should deal with one issue only. If your description of your submission includes words such as _and_, it is a big red warning sign that you should think about splitting your submission in two separate smaller submissions, if the changes are not intertwined.

# Coherent changes

Each submission should be a coherent whole: if you need to edit ten files to get a feature to work, then, the submission should contain all the changes for these ten files. Of course, if you can split the feature in sub-features, then, you should do it to decrease the size of the submission as per the previous item.

For example, if you have made changes to optimized a module and to fix a bug in another module, make sure you separate these two sets of changes in two separate submissions.

# Multi-part submissions

If you are working on a large new feature or a large refactoring, and because you will attempt to minimize the size of your submissions, you will have to split your large work in multiple separate submissions.

Ideally, these submissions will be started by a detailed explanation of the overall plan such that code reviewers can review each submission separately but within a large context. This kind of work typically is split in multiple dependent steps where each step depends on the previous one. If this is the case, make it very clear in your initial explanation. If you can, minimize dependencies between each step such that reviewers can merge each step separately without having to consider the impact of merging one submission on other submissions.

# History rewriting

The idea behind history rewriting is to request a code submitter to re-arrange their repository history prior to merging in the main ns-3 repository.

For example, let&#8217;s say that during development of a new feature, you decide to use your mercurial repository as a fancy version-enabled backup system: you do a lot of work, and regularly commit it to save it. When you are done implementing and testing that new feature, the resulting repository history as shown with <tt>hg log</tt> will look very verbose, and will most likely contain a lot of commit messages such as _fix bug_. Clearly, none of these commits are very helpful and there is little point in keeping them around: they make it painful to use the annotate, and bisect commands, and will make review of the final mercurial tree harder than it need to be.

A simple way to work around these problems is to ask each contributor to re-structure their commit history before submitting their tree for review:

  * pointless backup commits are deleted from the history 
      * commits are re-organized in sub-feature commits 
          * each commit is made buildable 
              * etc. </ul> 
                ## Rewrite a new history from scratch
                
                Once you are done with a new feature, you can generate one final patch with the mercurial <tt>diff</tt> command and apply it to a clean repository as one single commit with a nice new commit message.
                
                A variant on the above is to split the final commit in multiple smaller commits, each of which addresses one aspect of the final feature and make sure that each commit is still buildable.
                
                ## Build clean history from the start
                
                Instead of using mercurial as a powerful backup system and trying to go back later, some users prefer to use tools such as &#8216;mq&#8217; or &#8216;pbranch&#8217; to split their work from the start in a set of separate entities and still record somewhere their day-to-day development history.
                
                More information on these tools:
                
                  * [Managing change with mercurial queues](http://hgbook.red-bean.com/read/managing-change-with-mercurial-queues.html) 
                      * [Advanced uses of mercurial queues](http://hgbook.red-bean.com/read/advanced-uses-of-mercurial-queues.html) 
                          * The [Pbranch](http://arrenbrecht.ch/mercurial/pbranch/) extension. </ul> 
                            # Before sending a submission
                            
                            When you send a submission for review and merging in ns-3, before you hit the 'Send' button of your email client, ask yourself one last time: _If I were a reviewer, and I had to review that submission, what would I do ?_. Specifically, make sure that you have provided enough context to allow someone else to understand what you did and why. Keep in mind that your reviewer does not have access to a readable dump of your brain: he has access only to your code, and your emails.
