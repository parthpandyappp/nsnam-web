---
title: What is ns-3
layout: page
permalink: /about/what-is-ns-3/
---
ns-3 is a discrete-event network simulator, targeted primarily for research and educational use. ns-3 is free software, licensed under the [GNU GPLv2 license](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html), and is publicly available for research, development, and use.

The goal of the ns-3 project is to develop a free and open source simulation environment suitable for networking research: it should be aligned with the simulation needs of modern networking research and should encourage community contribution, peer review, and validation of the software.  ns-3 is maintained by a worldwide team of volunteer maintainers.

## Simulation models

The ns-3 project is committed to building a solid simulation core that is well documented, easy to use and debug, and that caters to the needs of the entire simulation workflow, from simulation configuration to trace collection and analysis.

Furthermore, the ns-3 software infrastructure encourages the development of simulation models that are sufficiently realistic to allow ns-3 to be used as a realtime network emulator, interconnected with the real world, and that allows many existing real-world protocol implementations to be reused within ns-3.

The ns-3 simulation core supports research on both IP and non-IP based networks. However, the large majority of its users focuses on wireless/IP simulations which involve models for Wi-Fi, LTE, or other wireless systems for layers 1 and 2.  Other popular research topics include TCP performance and mobile ad hoc routing protocol performance.

ns-3 also supports a real-time scheduler that facilitates a number of "simulation-in-the-loop" use cases for interacting with real systems.&nbsp; For instance, users can emit and receive ns-3-generated packets on real network devices, and ns-3 can serve as an interconnection framework to add link effects between virtual machines.

Another emphasis of the simulator is on the reuse of real application and kernel code.&nbsp; The Direct Code Execution framework allows users to run C or C++-based applications or the Linux kernel networking stack within ns-3.

## Community involvement

Because creating and maintaining a network simulator, with a number of high-fidelity, validated models of technologies that continue to evolve, requires a lot of work, ns-3 attempts to spread this workload over a large community of users and developers.

Roughly two or three times per year, we ship a new release of ns-3 with new models and extensions or bug fixes to previously released models.  We encourage the open validation of these models by third parties on our mailing-lists to ensure that the models we ship are and stay of the highest quality possible.
