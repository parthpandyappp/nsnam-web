---
title: Statistics
layout: page
permalink: /about/statistics/
---
This page lists some statistics about the project:

  * **Number of publications involving ns-3**: We do not maintain exact counts of publications.  <a href="https://scholar.google.com/scholar?q=%22ns-3%22++simulator&#038;hl=en&#038;as_sdt=1,48&#038;as_vis=1" target="_blank">Google Scholar</a> can be used to search for exhaustive lists of ns-3 publications, theses, technical reports, and references. A search of the &#8216;ns-3 simulator&#8217; results since 2017 (excluding patents and citations) yields **over 2000 links** (with some false positives). The IEEE digital library lists 145 ns-3 publications for 2017, and the ACM digital library lists 2579 publications matching the search term &#8216;ns-3&#8217; in 2017 (searched 1/22/18).
  * **ns-3-users group**: As of January 2018, there were **8183 members** of the [ns-3-users group](http://groups.google.com/group/ns-3-users). The number of posts to the ns-3-users group can be viewed  [at this web page](http://groups.google.com/group/ns-3-users/about). In 2017, there were approximately **700 posts/month**.
  * **ns-developers mailing list**: The [ns-developers mailing list](http://mailman.isi.edu/mailman/listinfo/ns-developers) is where software development discussions take place. As of January 2018, there were **1560 subscribers** to this list.
  * **Number of authors**: As of ns-3.27 release, **220 people** are listed as authors in the [AUTHORS file](http://code.nsnam.org/ns-3.27/file/a6d4461e1a3d/AUTHORS).
  * **Number of maintainers**: ns-3 maintainers have responsibility and authority to oversee and maintain different portions of the codebase. As of ns-3.27 release, **23 people** are [listed as ns-3 maintainers](http://www.nsnam.org/developers/maintainers/).
  * **Number of downloads**: The project has not been maintaining counts of downloads for the past few years.  In 2016 (the last year counted), ns-3 releases were downloaded approximately **72,000 times** from the main web server.

