jQuery(document).ready( function($) {

  //add helper class to sidebar sub-page navigation
  if ( jQuery('ul.side-nav').length ) {
    var menu_tiers = jQuery('ul.side-nav');
    menu_tiers.each(function(i){
      jQuery(this).addClass('level-' + i);
    });
  }

});
