---
layout: page
title: Keynote
permalink: /research/wns3/wns3-2014/keynote/
---
**Title: Slightly Out-of-the-Box Wireless Networking Research**

**Speaker: Prof. Ellen W. Zegura**, Chair of Computer Science at Georgia Tech

**Abstract:**

In this talk I describe several research projects in mobile and wireless networking that fall somewhat outside the mainstream. These projects fall into the areas of disruption tolerant networking, opportunistic computing and mobile high performance computing. They have in common that the properties of the wireless links and network are crucial to the performance of algorithms. I will discuss the methods of evaluation used by my group and others in this space and endeavor to suggest ways that wireless simulators such as ns-3 might meet part of the evaluation needs.
