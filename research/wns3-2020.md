---
layout: page
title: WNS3 2020
permalink: /research/wns3/wns3-2020/
---

The **Workshop on ns-3 (WNS3)** is a workshop to be held on June 17-18, 2020, hosted by the U.S. National Institute of Standards and Technology (NIST) in Gaithersburg, Maryland.  The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities. 

The WNS3 is part of a week-long annual meeting for ns-3, including **[ns-3 training](/research/wns3/wns3-2020/training/)** on June 15-16, and other activities related to ns-3 use and development.  On Thursday June 18, the annual meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/) will be held.

We plan to again publish the proceedings in the [ACM digital library](https://dl.acm.org/).

* [Call for Papers](/research/wns3/wns3-2020/call-for-papers/)
* [Local information and VISA](/research/wns3/wns3-2020/local-information/)
