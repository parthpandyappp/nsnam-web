---
layout: page
title: WNGW 2019
permalink: /research/wngw/wngw-2019/
---

The **Workshop on Next-Generation Wireless (WNGW)** with ns-3 was a 
specialized, single day workshop held
on June 21, 2019, following WNS3, hosted by the Department of Information Engineering, University of Florence, Italy.  

The goal of this workshop is to connect key ns-3 maintainers and developers with industrial and academic users of ns-3, so as to identify opportunities for collaborative development and peer review of ns-3 simulation models of advanced wireless technologies.

The WNGW was part of a week-long annual meeting for ns-3, including **[ns-3 training](/research/wns3/wns3-2019/training/)** on June 17-18, and the annual **[Workshop on ns-3](/research/wns3/wns3-2019/)** on Wednesday June 19.  On Thursday June 20, the annual meeting of the ns-3 Consortium
was held, as well as some software development discussions.

Extended abstracts of selected talks have been published in the [ACM digital library](https://dl.acm.org/citation.cfm?id=3337941).

* [Program committee](/research/wngw/wngw-2019/program-committee/)
