---
layout: page
title: WNS3 2014
permalink: /research/wns3/wns3-2014/
---
The Workshop on ns-3 (WNS3) is a one day workshop that was held on May 7, 2014, on the campus of the Georgia Institute of Technology in Atlanta GA. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

 **Meeting flyer:** [(pdf)](http://www.nsnam.org/wp-content/uploads/2014/02/ns-3-annual-2014.pdf)

**Important dates:**

Papers submission deadline : <s>January 31, 2014</s>&nbsp;February 7, 2014 (now closed)

[Poster/demo](http://www.nsnam.org/research/wns3/wns3-2014/call-for-posters/) submission deadline : April 20, 2014

Notification of acceptance : <s>February 28, 2014</s>

Camera-ready deadline : <s>April 4, 2014</s>

Workshop in Atlanta : May 7, 2014
