---
layout: page
title: Workshop on ns-3
permalink: /research/wns3/
---

The **Workshop on ns-3** is an annual academic workshop focusing on advances
to ns-3 and the design and performance of ns-3.  The workshop is organized
by the [ns-3 Consortium](/consortium/).   Proceedings are published in the
[ACM digital library](https://dl.acm.org).
