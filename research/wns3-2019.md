---
layout: page
title: WNS3 2019
permalink: /research/wns3/wns3-2019/
---

The **Workshop on ns-3 (WNS3)** was a single day workshop
held on June 19, 2019, hosted by the Department of Information Engineering, University of Florence, Italy.  The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities. 

The WNS3 is part of a week-long annual meeting for ns-3, including **[ns-3 training](/research/wns3/wns3-2019/training/)** on June 17-18, and a specialized **[workshop on next-generation wireless with ns-3](/research/wns3/wns3-2019/wngw/)** on Friday June 21.  On Thursday June 20, the annual meeting of the ns-3 Consortium was held, as well as some software development discussions.

Proceedings are published in the [ACM digital library](https://dl.acm.org/citation.cfm?id=3321349).

* [Workshop on next-generation wireless with ns-3](/research/wns3/wns3-2019/wngw/)
* [Local information and VISA](/research/wns3/wns3-2019/local-information/)
