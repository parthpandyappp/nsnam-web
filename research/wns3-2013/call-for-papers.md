---
layout: page
title: Call for Papers
permalink: /research/wns3/wns3-2013/call-for-papers/
---
The Workshop on ns-3 (WNS3) is a one day workshop held in conjunction with the sixth International Conference on Simulation Tools and Techniques ([SIMUTools 2013](http://simutools.org/2013/)). The workshop will be held on Tuesday March 5, 2013, in Cannes, France. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss about the ns-3 simulator and related activities.

WNS3 invites authors to submit original high quality papers presenting different aspects of developing and using ns-3. Accepted papers will be made available in the ACM Digital Library and EU Digital Library.

Topics of interest include, but are not limited to, the following:

  * new models, devices, protocols and applications for ns-3
  * using ns-3 in modern networking research
  * comparison with other network simulators and emulators
  * speed and scalability issues for ns-3
  * multiprocessor and distributed simulation with ns-3, including the use of GPUs
  * validation of ns-3 models
  * credibility and reproducibility issues for ns-3 simulations
  * user experience issues of ns-3
  * frameworks for the definition and automation of ns-3 simulations
  * post-processing, visualisation and statistical analysis tools for ns-3
  * models ported from other simulators to ns-3 and models ported from ns-3 to other simulation environments
  * using real code for simulation with ns-3 and using ns-3 code in network applications
  * integration of ns-3 with testbeds, emulators, and other simulators or tools
  * using ns-3 API from programming languages other than C++ or Python
  * porting ns-3 to unsupported platforms
  * network emulation with ns-3
  * ns-3 documentation issues
  * ns-3 community issues
  * using ns-3 in education and teaching to use ns-3

Papers must be written in English and must not exceed 8 pages. Authors should submit papers through [EasyChair](https://www.easychair.org/conferences/?conf=wns32013) in PDF format, complying with the ACM conference proceedings format. Submitted papers must not have been submitted for review or published (partially or completely) elsewhere. Every paper will be peer-reviewed. At least one author of each accepted paper must register and present the work at the conference.

### Acceptance Criteria

Papers will be accepted based on the relevance, novelty, and impact of the contribution, as well as the quality of writing and presentation.

Authors presenting new ns-3 models, frameworks, integration setups, etc. are encouraged to include all traditional parts of a scientific paper: introduction, motivation, related work, assumptions, verification and validation, conclusions and references. As a general rule, papers that only document source code will be rejected.

Authors presenting networking research using ns-3 are encouraged to follow best simulation practices and focus particularly on the credibility and reproducibility of simulation results. As a general rule, papers that only present simulation results obtained without any significant modification to the ns-3 simulator, and without a focus on simulation methodology, will be considered out of scope and rejected.

We strongly encourage authors of all papers, demonstrations, and posters to include links to relevant source code and instructions on how to use it. This will make contributions more useful for the ns-3 community. For papers presenting new ns-3 models, a link to the respective code review issue will be a plus.

Please do not hesitate to contact the workshop chairs if you are uncertain whether your submission falls within the scope of the workshop.

### Copyright Policy

Papers accepted to WNS3 and published in the proceedings (including posting to ACM Digital Library and to EU Digital Library) are required to perform a copyright transfer according to the ICST Copyright Policy. In the spirit of open source, we encourage authors of published papers to exercise their right to publish author-prepared versions on their respective home page, or on a publicly accessible server of their employer.

Authors of accepted papers are also allowed to choose not to publish their paper in the ACM Digital Library and EU Digital Library. In this case, the authors will not be required to transfer the copyright of their work to ICST, and they will be responsible for disseminating their work after the workshop.

### Demonstrations and Posters

In addition to the regular paper track, we are organising an exhibition-style demonstration and poster session, not to be published on the conference proceedings. The aim is to foster interactive discussions on work-in-progress, new problem statements, ideas for future development related to ns-3, and display innovative prototypes.

To propose a demonstration or poster, please submit a one or two page long extended abstract in PDF format to the TPC chairs. For demonstrations, the abstract should include the basic idea, the scope, and significance of the same. Additionally, please provide information about the equipment to be used for the demonstration and whether any special arrangements will be needed. Be as specific as possible in describing what you will demonstrate. Please include an estimate of the space, and setup time needed for your demonstration.

Accepted poster and demo abstracts will be published on the ns-3 web site. At least one author of each accepted demo/poster must register and present the work at the conference.

### Awards

One Best Paper, one Best Student Paper and one Best Demo or Poster will be selected through peer reviews and will be announced at the workshop. Only papers with a student as the first author are eligible for the Best Student Paper Award.

### Other Events

We are planning additional events (such as developers meeting and possibly other events) for the days around the workshop event. More information on these will be announced at a future date.

### Important Dates

Papers submission deadline : <del>December 5, 2012</del> <del>December 19, 2012</del>

Notification of acceptance : <del>January 16, 2013</del>

Camera-ready deadline : February 7, 2013

Demos and posters proposal deadline : February 13, 2013

Workshop in Cannes: March 5, 2013
