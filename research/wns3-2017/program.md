---
layout: page
title: WNS3 2017 Program
permalink: /research/wns3/wns3-2017/program/
---

# 13th June (Tuesday), 2017
Opening Ceremony



**09:00-10:00 Transmission Control Protocol (session chair: Michele Polese)**

- A SACK-based Conservative Loss Recovery Algorithm for ns-3 TCP: a Linux-inspired Proposal (Natale Patriciello)
- Common TCP Evaluation Suite for ns-3: Design, Implementation and Open Issues (Kriti Nagori, Meenakshy Balachandran, Ankit Deepak, Mohit P. Tahiliani, and B.R Chandavarkar)

**10:00-10:30** Coffee Break

**10:30-12:00 WiFi (session chair: Mohit P. Tahiliani)**

- Link-to-System Mapping for ns-3 Wi-Fi OFDM Error Models (Rohan Patidar, Sumit Roy, Thomas Henderson, and Amrutha Chandramohan)

- Extending the IEEE 802.11ad Model: Scheduled Access, Spatial Reuse, Clustering, and Relaying (Hany Assasa and Joerg Widmer)

- Exploiting the Capture Effect on DSC and BSS Color in Dense IEEE 802.11ax Deployments (Ioannis Selinis, Konstantinos Katsaros, Seiamak Vahid, and Rahim Tafazolli)

**12:00-13:30** Lunch

**13:30-15:00 LTE/5G (session chair: Hany Assasa)**

- Implementation and Validation of an LTE D2D Model for ns-3 (Richard Rouil, Fernando J. Cintr&oacute;n, Aziza Ben Mosbah, and Samantha Gamboa)

- Towards LTE-Advanced and LTE-A Pro Network Simulations: Implementing Carrier Aggregation in LTE Module of ns-3 (Biljana Bojovic, Melchiorre Danilo Abrignani, Marco Miozzo, Lorenza Giupponi, and Nicola Baldo)

- ns-3 Implementation of the 3GPP MIMO Channel Model for Frequency Spectrum above 6 GHz (Menglei Zhang, Michele Polese, Marco Mezzavilla, Sundeep Rangan, and Michele Zorzi)

**15:00-15:30** Coffee Break

**15:30-16:30 Transport Protocols (session chair: Natale Patriciello)**

- An Implementation and Analysis of SCPS-TP in ns-3 (Truc Anh Nguyen and James Sterbenz)

- Support Multiple Auxiliary TCP/UDP Connections in SDN Simulations Based on ns-3 (Hemin Yang, Chuanji Zhang, and George Riley)

**16:30-18:00 Posters/Demos/Works-in-Progres<wbr />s Session**

# 14th June (Wednesday), 2017

**08:30-10:00 Application Traffic and Control (session chair: Helder M. Fontes)**

- Traffic Differentiation and Multiqueue Networking in ns-3 (Pasquale Imputato and Stefano Avallone)

- Design and Implementation of AQM Evaluation Suite for ns-3 (Ankit Deepak, Shravya K.S., and Mohit Tahiliani)

- Simulation Framework for HTTP-Based Adaptive Streaming Applications (Harald Ott, Konstantin Miller, and Adam Wolisz)

**10:00-10:30** Coffee Break

**10:30-12:00 Other Topics (session chair: Biljana Bojović)**

- Kinematic Constraints and ns-3 Mobility Models: the AUV Issue (Matteo Franchi, Tommaso Pecorella, Alessandro Ridolfi, Romano Fantacci, and Benedetto Allotta)

- Simulation Module and Tools for XDense Sensor Network (Jo&atilde;o Loureiro, Pedro Santos, Raghuraman Rangarajan, and Eduardo Tovar)

- A Trace-based ns-3 Simulation Approach for Perpetuating Real-World Experiments (Helder Fontes, Rui Campos, and Manuel Ricardo)
