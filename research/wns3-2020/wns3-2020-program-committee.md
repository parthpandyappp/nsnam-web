---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2020/program-committee/
---

# General Chair

* Richard Rouil, NIST (richard.rouil at nist.gov)

# Technical Program Co-Chairs

* Stefano Avallone (stavallo at unina.it)
* Matthieu Coudron (mattator at gmail.com)

# Proceedings Chair

* Eric Gamess, Jacksonville State University (egamess at gmail.com)

# Technical Program Committee

To be announced.
