---
layout: page
title: About
permalink: /about/
---

## What is ns-3?
ns-3 is a discrete-event network simulator, targeted primarily for research and educational use. ns-3 is free software, licensed under the GNU GPLv2 license, and is publicly available for research, development, and use.

The goal of the ns-3 project is to develop a preferred, open simulation environment for networking research: it should be aligned with the simulation needs of modern networking research and should encourage community contribution, peer review, and validation of the software.

### Simulation Models
The ns-3 project is committed to building a solid simulation core that is well documented, easy to use and debug, and that caters to the needs of the entire simulation workflow, from simulation configuration to trace collection and analysis.

Furthermore, the ns-3 software infrastructure encourages the development of simulation models which are sufficiently realistic to allow ns-3 to be used as a realtime network emulator, interconnected with the real world and which allows many existing real-world protocol implementations to be reused within ns-3.

The ns-3 simulation core supports research on both IP and non-IP based networks. However, the large majority of its users focuses on wireless/IP simulations which involve models for Wi-Fi, WiMAX, or LTE for layers 1 and 2 and a variety of static or dynamic routing protocols such as OLSR and AODV for IP-based applications.

ns-3 also supports a real-time scheduler that facilitates a number of "simulation-in-the-loop" use cases for interacting with real systems.  For instance, users can emit and receive ns-3-generated packets on real network devices, and ns-3 can serve as an interconnection framework to add link effects between virtual machines.

Another emphasis of the simulator is on the reuse of real application and kernel code.  Frameworks for running unmodified applications or the entire Linux kernel networking stack within ns-3 are presently being tested and evaluated.

### Community Involvement
Because creating a network simulator that sports a sufficient number of high-quality validated, and maintained models requires a lot of work, ns-3 attempts to spread this workload over a large community of users and developers.

Every three months, we ship a new stable version of ns-3 with new models developed, documented, validated, and maintained by enthusiastic researchers. We encourage the open validation of these models by third parties on our mailing-lists to ensure that the models we ship are and stay of the highest quality possible.
