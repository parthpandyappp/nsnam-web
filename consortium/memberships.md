---
title: Memberships
layout: page
permalink: /consortium/memberships/
---
Consortium Members are of four kinds:
  * Universities, non-profits, FFRDCs
  * Very small companies (fewer than 20 employees)
  * Small companies (20-500 employees)
  * Large companies (greater than 500 employees)

Prospective members should read the [NS-3 Consortium Affiliation and Membership Agreement](/docs/consortium/NS3_Consortium_Agreement_Bylaws.020619.pdf) and inquire at
<consortium@nsnam.org> about joining.

