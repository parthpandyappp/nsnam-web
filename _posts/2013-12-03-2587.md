---
id: 2587
title: ns-3 workshop at NITK, Surathkal
date: 2013-12-03T19:47:50+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2587
permalink: /events/2587/
categories:
  - Events
---
The Wireless Information Networking Group (WiNG) at NITK, Surathkal will host a Workshop on Network Simulation using ns-3 on December 7-8, 2013. For more information about the workshop, please visit  [the web site](http://wing.nitk.ac.in) or write to <wing@nitk.ac.in> or  <nitkwing@gmail.com>.