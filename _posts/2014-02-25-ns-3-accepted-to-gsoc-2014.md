---
id: 2757
title: ns-3 accepted to GSoC 2014
date: 2014-02-25T13:54:25+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2757
permalink: /news/ns-3-accepted-to-gsoc-2014/
categories:
  - Events
  - News
---
Our project was pleased to learn today that we will be one of 190 organizations participating in the 2014 [Google Summer of Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2014). This program is a great opportunity for students to learn a bit about software engineering and open source projects, and for our project community to grow. Interested students are encouraged to interact with the project through the main project mailing list, [ns-developers@isi.edu](http://www.nsnam.org/developers/tools/mailing-lists/), and to review our [wiki](http://www.nsnam.org/wiki/index.php/GSOC2014StudentGuide). Students will have until March 21 to learn about ns-3, develop a project proposal, and submit it to Google.