---
id: 1957
title: NetAnim 3.103
date: 2013-01-08T05:00:16+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1957
permalink: /news/netanim-3-103/
categories:
  - News
---
NetAnim (Version 3.103) was bundled with ns-3.16. It is also available via mercurial by using:&#8221;hg clone http://code.nsnam.org/netanim&#8221;. The release notes are available at [Release Notes](http://code.nsnam.org/netanim/file/4990cf2177a1/RELEASE_NOTES "Release Notes"). For more details refer the [wiki page](http://www.nsnam.org/wiki/index.php/NetAnim "Wiki Page").

&nbsp;

Some of the features available in this version are:

1. Tabulating Node positions (X,Y co-ordinates) vs time.
  
2. Plotting the path of a mobile node vs time. This may be useful to visualize the distance between 2 nodes at various simulation time.

<div>
  <div id="attachment_1959" style="width: 310px" class="wp-caption alignleft">
    <a href="http://www.nsnam.org/news/netanim-3-103/attachment/615px-trajectory/" rel="attachment wp-att-1959"><img class="size-medium wp-image-1959" title="Node position and trajectory" src="http://www.nsnam.org/wp-content/uploads/2013/01/615px-Trajectory-300x292.png" alt="" width="300" height="292" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/615px-Trajectory-300x292.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/615px-Trajectory-150x146.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/615px-Trajectory.png 615w" sizes="(max-width: 300px) 100vw, 300px" /></a>
    
    <p class="wp-caption-text">
      Node position and trajectory
    </p>
  </div>
</div>

3. Tabulating packet transmission and reception between nodes vs time.

<div>
  <a href="http://www.nsnam.org/news/netanim-3-103/attachment/packetstats/" rel="attachment wp-att-1967"><img class="size-medium wp-image-1967" title="Packet timeline" src="http://www.nsnam.org/wp-content/uploads/2013/01/PacketStats-300x213.png" alt="" width="300" height="213" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/PacketStats-300x213.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/PacketStats-150x106.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/PacketStats.png 628w" sizes="(max-width: 300px) 100vw, 300px" /></a>
</div>

&nbsp;

4. Displaying brief packet meta-data with the ability to do regex filtering on the text

<div id="attachment_1969" style="width: 280px" class="wp-caption alignleft">
  <a href="http://www.nsnam.org/news/netanim-3-103/attachment/tcp/" rel="attachment wp-att-1969"><img class="size-full wp-image-1969" title="Packet Meta data" src="http://www.nsnam.org/wp-content/uploads/2013/01/TCP.png" alt="" width="270" height="233" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/TCP.png 270w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/TCP-150x129.png 150w" sizes="(max-width: 270px) 100vw, 270px" /></a>
  
  <p class="wp-caption-text">
    Packet Meta data
  </p>
</div>

5. Ability to go back and forward smoothly in simulation time using the Sim-time slider
  
6. Support for setting the following attributes before simulation and modifying them during simulation

  * P2p Link Description
  * Color of the nodes
  * Visibility of the nodes
  * Description of the nodes (Default is Node ID)

<div id="attachment_1972" style="width: 310px" class="wp-caption alignleft">
  <a href="http://www.nsnam.org/news/netanim-3-103/attachment/linkdescription/" rel="attachment wp-att-1972"><img class="size-medium wp-image-1972" title="Attributes" src="http://www.nsnam.org/wp-content/uploads/2013/01/LinkDescription-300x199.png" alt="" width="300" height="199" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/LinkDescription-300x199.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/LinkDescription-150x99.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/LinkDescription.png 611w" sizes="(max-width: 300px) 100vw, 300px" /></a>
  
  <p class="wp-caption-text">
    Node color, link description, Node description
  </p>
</div>

&nbsp;

<a href="http://www.nsnam.org/news/netanim-3-103/attachment/nodecolorvisibility/" rel="attachment wp-att-1974"><img class="size-medium wp-image-1974" title="Hidden Node" src="http://www.nsnam.org/wp-content/uploads/2013/01/NodeColorVisibility-300x190.png" alt="" width="300" height="190" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/NodeColorVisibility-300x190.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/NodeColorVisibility-150x95.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/NodeColorVisibility.png 603w" sizes="(max-width: 300px) 100vw, 300px" /></a>

<dl id="attachment_1974" class="wp-caption alignleft" style="width: 310px;">
  <dd class="wp-caption-dd">
    Hide a node                                                       
  </dd>
</dl>

&nbsp;

7. Built-in support for printing the IP and MAC at the interfaces. Currently if there is a text overlap, the IP/MAC text will be positioned outside the scene and connected by light blue line, to prevent clutter.

<div id="attachment_1976" style="width: 310px" class="wp-caption alignleft">
  <a href="http://www.nsnam.org/news/netanim-3-103/attachment/ip/" rel="attachment wp-att-1976"><img class="size-medium wp-image-1976" title="IPv4 Address on interface" src="http://www.nsnam.org/wp-content/uploads/2013/01/IP-300x244.png" alt="" width="300" height="244" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/IP-300x244.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/IP-150x122.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/IP.png 708w" sizes="(max-width: 300px) 100vw, 300px" /></a>
  
  <p class="wp-caption-text">
    IPv4 Address on interface
  </p>
</div>

<div id="attachment_1978" style="width: 310px" class="wp-caption alignleft">
  <a href="http://www.nsnam.org/news/netanim-3-103/attachment/mac/" rel="attachment wp-att-1978"><img class="size-medium wp-image-1978" title="MAC Address on interface" src="http://www.nsnam.org/wp-content/uploads/2013/01/MAC-300x206.png" alt="" width="300" height="206" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/MAC-300x206.png 300w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/MAC-150x103.png 150w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2013/01/MAC.png 827w" sizes="(max-width: 300px) 100vw, 300px" /></a>
  
  <p class="wp-caption-text">
    MAC Address on interface
  </p>
</div>

&nbsp;

8. XML Tags for packet and wpacket now support &#8220;p&#8221; and &#8220;wp&#8221; respectively (instead of &#8220;packet&#8221; and &#8220;wpacket&#8221;), to save disk-space.
  
9 Ability to capture the animation trace file for a given time slot rather than the entire simulation