---
id: 3963
title: ns-3.28.1 released
date: 2018-08-16T04:30:10+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3963
permalink: /news/ns-3-28-1-released/
categories:
  - Events
  - News
  - ns-3 Releases
---
A minor release of ns-3.28, numbered ns-3.28.1 was posted on August 16, 2018, mainly to address some [build issues](https://www.nsnam.org/wiki/Ns-3.28-errata) with Fedora 28. The API scanning process has also been further automated. The release is available [here](https://www.nsnam.org/ns-3-28/download/).