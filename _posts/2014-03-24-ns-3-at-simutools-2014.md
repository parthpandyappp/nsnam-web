---
id: 2796
title: ns-3 at SIMUTools 2014
date: 2014-03-24T15:40:38+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2796
permalink: /news/ns-3-at-simutools-2014/
categories:
  - News
---
At the seventh edition of the annual [SIMUTools](http://simutools.org/2014/) international conference, two papers and a poster were presented that described new ns-3 models, and two posters and a paper used ns-3 in their work. The conference program is posted [here](http://simutools.org/2014/index.php?n=Program.PreliminaryProgram).

A wireless bursty link model was proposed in a paper entitled &#8220;Simulating frame-level bursty link in wireless networks&#8221;. The authors proposed a stochastic bursty link model to simulate bursty behavior observed in real wireless communications. The model works by adjusting the probability of successfully receiving a frame based on the history of the link. The authors showed that the model can simulate different bursty behaviors observed in real wireless links and that burstiness affects the performance of a wireless routing protocol.

A satellite model for ns-3 was proposed in a paper entitled &#8220;Satellite model for network simulator 3&#8221;. The authors proposed a satellite extension for ns-3 based on DVB-S2 and DVB-RCS2 specifications for forward and return links, respectively. The authors designed the module to be highly flexible and to include Phy and Channel models.

A service-oriented router model was proposed in a poster entitled &#8220;Service-oriented router module implementation on ns-3&#8221;. The authors proposed a service-oriented router (SoR) that provides content-based services. The router works by looking at packet content and selects the route based on the content. The authors implemented the proposed model in ns-3 and showed that the model does not incur significant processing load and memory usage.

ns-3 was used as a baseline for comparison in a paper entitled &#8220;VMSimInt: a network simulation tool supporting integration of arbitrary kernels and application&#8221; where the authors proposed a new approach to integrate arbitrary operating system and application code into an event-driven network simulator. Instead of integrating parts of OS kernels with the simulator, VMSimInt uses virtual machine, which provides more flexibility since the OS can be used as-is. The authors validated the model against ns-3/NSC and showed that VMSimInt is able to simulate the same behavior.

In a poster entitled &#8220;Simplified model of wireless network throughput changes resulting from client mobility&#8221;, the authors proposed a simplified model to directly simulating the changes of bitrate and packet loss ratio without full representation of nodes&#8217; locations. The authors showed that the technique is able to significantly reduce the simulation computation time. The model was based on real measurements. The authors validated the model by comparing the model with ns-3 with full signal propagation calculations.

In a poster entitled &#8220;Co-simulation solutions using AA4MM-FMI applied to smart space heating models&#8221;, the authors proposed to applt the AA4MM framework to the co-simulation of a smart space heating environment. The authors used ns-3 as a network simulator in the co-simulation framework.