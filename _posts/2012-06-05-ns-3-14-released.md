---
id: 1715
title: ns-3.14 released
date: 2012-06-05T05:39:05+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1715
permalink: /news/ns-3-14-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.14 was released on June 5, 2012. A significant amount of work went into the ns-3.14 release on several fronts:

  * The transport protocol implementations for IP have been refactored so that they can support IPv4 and IPv6 connections
  * The latest release of [the LENA project](http://iptechwiki.cttc.es/LTE-EPC_Network_Simulator_%28LENA%29) has been incorporated. The LTE module has been reworked, with new LTE radio protocol models and an EPC data plane model. New propagation models (OkumuraHata, ITU-R P.1411, ITU-R P.1238), and a buildings pathloss model, have also been added. An antenna module, which includes different radiation pattern models, has been added.
  * The [Random Early Detection (RED)](http://www.icir.org/floyd/red.html) queue model from ns-2 has been ported
  * A [Dynamic Source Routing (DSR)](http://www.ietf.org/rfc/rfc4728.txt) protocol model for IPv4 has been added
  * An alternative implementation of [the Jakes propagation loss model](http://www.nsnam.org/docs/release/3.14/doxygen/classns3_1_1_jakes_propagation_loss_model.html) was added, to include narrowband (or frequency non-selective) cases of multipath propagation
  * The [netanim animator](http://www.nsnam.org/wiki/index.php/NetAnim) is now bundled with the release and has been added to the documentation