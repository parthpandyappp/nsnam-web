---
id: 2682
title: 2014 Workshop on ns-3 announced
date: 2013-12-23T23:17:57+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2682
permalink: /news/2014-workshop-on-ns-3-announced/
categories:
  - Events
  - News
---
The NS-3 Consortium is organizing the 2014 edition of the [Workshop on ns-3 (WNS3)](http://www.nsnam.org/wns3/wns3-2014/call-for-papers/), a one day workshop to be held on May 7, 2014, on the campus of the Georgia Institute of Technology in Atlanta GA. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 invites authors to submit original high quality papers presenting different aspects of developing and using ns-3. The Workshop is making arrangements with the ACM Digital Library and EU Digital Library to have papers published in the Digital Library, if the authors choose to do so. 

WNS3 will be part of a week-long series of events in Atlanta GA including ns-3 training, the consortium annual meeting, and developer meetings.