---
id: 2390
title: ns-3 GSoC students announced
date: 2013-05-27T20:13:17+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2390
permalink: /news/ns-3-gsoc-students-announced/
categories:
  - News
---
The ns-3 project and Google Summer of Code today accepted the applications of students Junling Bu and Budiarto Herman to participate in the [2013 Google Summer of Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2013).

Junling Bu, with his proposal titled &#8220;Implementation of WAVE 1609.4/802.11p&#8221;, will be mentored by Guillaume Rémy. WAVE is the IEEE standard for communications in vehicular ad-hoc networks (VANETs). This standard defines how vehicles can communicate together, and how they can communicate with road-side units (RSUs). The physical layer used in this protocol (802.11p) is similar to 802.11a (the 10MHz version). Higher layers introduce multi-channel operations that deal with specific VANET constraints (low-latency for safety applications, high mobility of vehicles, &#8230;). Junling Bu has been selected to work on this project during this summer: he will implement all layers needed to simulate an accurate WAVE network, beginning with lower MAC layer, and up to the channel management mechanisms.

Budiarto Herman, with his project titled &#8220;Improvements to UE Measurements in Active and Idle Mode for the LTE Module&#8221;, will be mentored by Nicola Baldo, Marco Miozzo, Manuel Requena and Jaime Ferragut. LTE is today&#8217;s cutting edge mobile communications technology, and the ns-3 LTE module is already quite popular among researchers and engineers working on the design of advanced LTE solutions. However, the ns-3 LTE module lacks some features for the simulation of advanced handover scenarios, in particular the support for some types of UE measurements, cell selection in idle mode, and a broader range of state-of-the-art handover algorithms. The goal of this project is to implement these missing features. If successful, this project will help ns-3 users and development in the simulation of LTE handover scenarios and the design and evaluation of innovative handover algorithms.