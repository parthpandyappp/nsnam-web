---
id: 3455
title: ns-3.25 released
date: 2016-03-24T07:00:24+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3455
permalink: /news/ns-3-25-released/
categories:
  - Events
  - News
---
ns-3.25 was released on 24 March 2016 and features the following significant changes. 

  * A new traffic control framework, inspired by the Linux traffic control subsystem, has been introduced to allow experimentation with Internet-aware active queue management (AQM) techniques, packet filtering, and policing. The existing network device queues were reworked, a Linux-like pfifo_fast queuing discipline was added, and existing AQM queue models (CoDel, RED) were ported to the new framework. The RED queue model was extended to support Adaptive RED.
  * The Wi-Fi module adds additional support for 802.11n and 802.11ac modes, including better support for larger channel widths and multiple spatial streams (MIMO), and a simplified helper API for MPDU and MSDU aggregation. Two adaptive rate controls for 802.11n/ac, Ideal and MinstrelHt, have been added. Finally, backward compatibility between 802.11g access points and 802.11b stations, and between 802.11n/ac and legacy stations, has been added.
  * The Internet module features a refactored TCP model to better support testing and to support modular congestion control classes. A RIPv2 routing protocol implementation was also added to the Internet module. 

Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.25/file/c43637263fc9/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.