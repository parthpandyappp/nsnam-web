---
id: 1399
title: ns-3.12 released
date: 2011-08-31T19:48:18+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1399
permalink: /news/ns-3-12-released/
categories:
  - News
  - ns-3 Releases
---
[ns-3.12](/releases/ns-3.12), featuring new support for OS X Lion, is a maintenance-oriented release that also adds a few new features such as fragmentation support for IPv4 and the ability to reuse single-frequency propagation loss models along with the frequency-dependent loss models for the [spectrum device and channel modeling framework](http://iptechwiki.cttc.es/Ns3_Spectrum). Full details are available in the [RELEASE_NOTES](http://code.nsnam.org/ns-3.12/raw-file/adb7521e467f/RELEASE_NOTES).

**Update:** Please note that a minor maintenance release, ns-3.12.1, was made on 2 September 2011 to fix a bug in the Python bindings for the PyViz visualizer.