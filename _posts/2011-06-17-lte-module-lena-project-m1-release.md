---
id: 1281
title: 'LTE module: LENA project M1 release'
date: 2011-06-17T17:00:34+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1281
permalink: /news/lte-module-lena-project-m1-release/
categories:
  - News
---
CTTC&#8217;s ns-3 developers team [announced](http://mailman.isi.edu/pipermail/ns-developers/2011-June/009113.html) the Milestone 1 (M1) release of the LTE module developed within the LTE-EPC Network Simulator (LENA) project.

Here is a summary of the new features available in the LTE module with this release:

  * uplink PHY and MAC properly implemented including Adaptive Modulation and Coding (previously only downlink was supported)
  * interference modeling was implemented: it is now possible to simulate multi-cell scenarios with inter-cell interference
  * flexible spectrum model: it is now possible to specify the carrier frequency and bandwidth of each cell independently
  * an implementation of the Proportional Fair scheduler based on the FF LTE MAC Scheduler API is now included
  * support for output of RLC and MAC layer Key Performance Indicators (KPIs) via dedicated trace sinks connected to newly defined trace
  
    sources
  * the setting of the most relevant parameters (e.g. tx power, noise, carrier frequency, bandwidth, type of scheduler, etc.) is now done via the ns-3 attribute system
  * several test suites have been added, including both unit tests that check the correct functionality of specific objects, and system tests that validate the output produced by the simulator in several scenarios of interest.
  * the documentation of the lte module has been entirely rewritten, it now features three separate sections: the design documentation, the user documentation and the testing documentation.