---
id: 2744
title: ns-3 training registration announced
date: 2014-02-19T16:08:21+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2744
permalink: /events/ns-3-training-registration-announced/
categories:
  - Events
---
The [ns-3 Consortium](http://www.nsnam.org/consortium/about/) will offer two days of user training on May 5-6, 2014 in Atlanta GA as part of its annual meeting, immediately preceding the Workshop on ns-3 on May 7. The two days of training, taught by several ns-3 maintainers, will be organized around the basic simulator on Monday and more advanced topics and extensions on Tuesday. A portion of each day will be reserved for interactive Q&A and guidance from the instructors, allowing deeper treatment of topics of particular interest. Additional details are available [here](http://www.nsnam.org/wp-content/uploads/2014/02/ns-3-training-2014.pdf) and registration can be made [here](http://ns3-annual-2014.eventzilla.net/).