---
id: 3407
title: Workshop on Wireless Network Performance Evaluation
date: 2016-02-22T20:04:09+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3407
permalink: /events/workshop-on-wireless-network-performance-evaluation/
categories:
  - Events
---
As part of the <a href="/wp-content/uploads/2016/02/ns-3-2016-save-the-date.pdf" target="_blank">ns-3 annual meeting</a>, the University of Washington will host a new workshop on <a href="/wp-content/uploads/2016/02/WNPE-call-for-presentations.pdf" target="_blank">Wireless Network Performance Evaluation</a> on Friday June 17. This single-day workshop will immediately follow the annual <a href="/overview/wns3/wns3-2016/" target="_blank">Workshop on ns-3</a>, and will consist of a series of invited and open-call presentations and a panel discussion concerning the evolution of tools and frameworks for supporting research on wireless networks, including ns-3 and also testbeds and research hardware.