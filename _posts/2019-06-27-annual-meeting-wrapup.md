---
title: Annual meeting wrapup
date: 2019-06-27T14:00:00+00:00
author: tomh
layout: post
permalink: /news/annual-meeting-wrapup-2019/
categories:
  - Events
  - News
---
Fifty-three attendees met at the University of Florence from June 17-21 for the <a href="https://www.nsnam.org/overview/wns3/wns3-2019/" target="_blank">11th annual Workshop on ns-3</a>, ns-3 training sessions, and <a href="https://www.nsnam.org/research/wngw/wngw-2019/" target="_blank">Workshop on Next-Generation Wireless Networks with ns--3</a>.  The meeting was organized by the <a href="https://www.nsnam.org/consortium/" target="_blank">ns-3 Consortium</a>, which held its annual plenary meeting on June 20. The Workshop on ns-3 featured ten paper presentations and several demo, poster, and work-in-progress talks on Wednesday, at which a team from University of Padova accepted the award for best paper regarding a QUIC module for ns-3.  Next year&#8217;s annual meeting is planned for North America (site and date TBD).
