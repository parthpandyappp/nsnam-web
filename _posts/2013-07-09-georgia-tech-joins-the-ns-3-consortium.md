---
id: 2426
title: Georgia Tech joins the NS-3 Consortium
date: 2013-07-09T13:57:57+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2426
permalink: /news/georgia-tech-joins-the-ns-3-consortium/
categories:
  - News
---
We&#8217;re pleased to announce that the [Georgia Institute of Technology](http://www.ece.gatech.edu) has joined the NS-3 Consortium as an Executive Member. Dr. George Riley, one of the founders of the open source project, has joined the Steering Committee. Georgia Tech has actively supported ns-3 since the project&#8217;s inception in many ways, including financial support, project infrastructure, and significant software contributions.