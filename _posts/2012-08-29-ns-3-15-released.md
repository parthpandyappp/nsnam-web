---
id: 1739
title: ns-3.15 released
date: 2012-08-29T16:24:16+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1739
permalink: /news/ns-3-15-released/
categories:
  - Events
  - News
  - ns-3 Releases
---
ns-3.15 was released on August 29, 2012. ns-3.15 is mainly a maintenance release. The following changes have been made since ns-3.14:

  * The random variable implementation has been refactored to allow users to control the mapping of underlying pseudo-random sequences, or streams, to random variable objects. This will allow users better control over keeping some aspects of a simulation scenario fixed (e.g. mobility) while allowing other aspects to vary in different experimental trials. The random variable objects themselves have been aligned with the ns-3 Object framework, including smart pointers and attributes.
  * Several smaller bugs in various ns-3 models have been fixed.