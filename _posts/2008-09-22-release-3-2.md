---
id: 490
title: Release 3.2
date: 2008-09-22T08:16:35+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=490
permalink: /news/release-3-2/
categories:
  - News
  - ns-3 Releases
---
## Availability

This release is immediately available from[here](http://www.nsnam.org/release/ns-3.2.tar.bz2).

## Supported platforms

ns-3.2 has been tested on the following platforms:

  * linux x86 gcc 4.2, 4.1, and, 3.4.6.
  * linux x86_64 gcc 4.3.2, 4.2.3, 4.2.1, 4.1.3, 3.4.6
  * MacOS X ppc and x86
  * cygwin gcc 3.4.4 (debug only)

Not all ns-3 options are available on all platforms; consult the [Installation](/wiki/Installation) page for more information.

## New user-visible features

  * Learning bridge (IEEE 802.1D): it is now possible to bridge together multiple layer 2 devices to create larger layer 2 networks. The Wifi and Csma models support this new mode of operation. (contributed by Gustavo Carneiro)
  * Python bindings: it is now possible to write simulation scripts in python using our python bindings (contributed by Gustavo Carneiro).
  * Real-time simulator: it is now possible to run simulations synchronized on the real-world wall-clock time (contributed by Craig Dowell).
  * Network Simulation Cradle: it is now possible to use the Network Simulation Cradle (http://www.wand.net.nz/~stj2/nsc/) in ns-3 and run simulations using various versions of kernel TCP network stacks. (contributed by Florian Westphal as part of his Google Summer of Code work)
  * A statistics framework: Joseph Kopena contributed a statistics framework which can be used keep track of simulation data in persistent storage across multiple runs (database and ascii file backends are available). More information on the wiki [here](/wiki/Statistical_Framework_for_Network_Simulation)

## Known issues

ns-3 build is known to fail on the following platforms:

  * gcc 3.3 and earlier
  * optimized builds on gcc 3.4.4 and 3.4.5
  * optimized builds on linux x86 gcc 4.0.x
  * optimized builds on Ubuntu 8.10 alpha 5 x86 gcc4.3.2
  * MinGW
