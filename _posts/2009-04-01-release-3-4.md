---
id: 486
title: Release 3.4
date: 2009-04-01T08:14:22+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=486
permalink: /news/release-3-4/
categories:
  - News
  - ns-3 Releases
---
## Availability

This release is immediately available from: [here](http://www.nsnam.org/release/ns-allinone-3.4.tar.bz2).

## Supported platforms

ns-3.4 has been tested on the following platforms:

  * linux x86 gcc 4.2, 4.1, and, 3.4.6.
  * linux x86_64 gcc 4.3.2, 4.2.3, 4.2.1, 4.1.3, 3.4.6
  * MacOS X ppc and x86
  * cygwin gcc 3.4.4 (debug only)

Not all ns-3 options are available on all platforms; consult the [Installation](/wiki/Installation) page for more information.

## New user-visible features

  * Wifi models: Timo Bingman contributed a ThreeLogDistance and a Nakagami propagation loss model based on the ns-2 models. Fabian Mauchle contributed multicast support.
  * Object Name Service: A facility allowing ns-3 Objects to be assigned names has been added.
  * Tap Bridge: A second option for integrating ns-3 with real-world hosts has been added. This allows for real hosts to talk over ns-3 net devices and simulated networks.
  * A new build option (ns-3-allinone) has been provided to make it easier for users to download and bulid commonly used ns-3 configurations.
  * The ns-2 calendar queue scheduler has been ported to ns-3.
  * XML support has been added to the ConfigStore.

## Known issues

ns-3 build is known to fail on the following platforms:

  * gcc 3.3 and earlier
  * optimized builds on gcc 3.4.4 and 3.4.5
  * optimized builds on linux x86 gcc 4.0.x
  * optimized builds on Ubuntu 8.10 alpha 5 x86 gcc4.3.2
  * MinGW

## Future releases

Our next release, which is expected to happen in 2 to 4 months from now, will feature the merging of some of our projects currently in development including fuller IPv6 support, and IPv4 and routing protocol refactoring, and some smaller features such as a new Global ARP package and possibly a new Testing and Validation suite,
