---
id: 3289
title: ns-3.24 released
date: 2015-09-15T23:07:36+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3289
permalink: /news/ns-3-24-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.24 was released on 15 September 2015. The main new model extension for this release is the addition of 802.11ac Very High Throughput (VHT) physical layer modes for Wi-Fi. This release also includes updates to the Waf build system and initial support for Python 3. The IPv4 ARP cache now supports the manual removal of entries and the addition of permanent entries. The SimpleChannel in the &#8216;network&#8217; module now allows per-NetDevice blacklists, in order to support hidden terminal testcases. Finally, the release includes about forty bug fixes and small improvements, listed in the [RELEASE_NOTES](http://code.nsnam.org/ns-3.24/file/dbd1a2b1c4ed/RELEASE_NOTES).