---
id: 313
title: Pyviz added to ns-3
date: 2011-01-14T17:40:42+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=313
permalink: /news/pyviz-added-to-ns-3/
categories:
  - News
---
As of ns-3.10, Gustavo Carneiro's pyviz has been added to ns-3.&nbsp; NS-3 PyViz is a live simulation visualizer, meaning that it uses no trace files. It can be most useful for debugging purposes, i.e. to figure out if mobility models are what you expect, where packets are being dropped, etc. There's also a builtin interactive python console that can be used to debug the state of the running objects. Although it is mostly written in Python, it works both with Python and pure C++ simulations.

Here is a typical screencap:

<p style="text-align: center;">
  <a href="http://www.nsnam.org/wp-content/uploads/2011/01/pyviz1.png"><img alt="" class="alignnone size-full wp-image-316" src="http://www.nsnam.org/wp-content/uploads/2011/01/pyviz1.png" style="width: 450px; height: 505px;" title="pyviz" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/pyviz1.png 625w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/pyviz1-267x300.png 267w" sizes="(max-width: 625px) 100vw, 625px" /></a>
</p>

Additional information can be found at the following URL:&nbsp; http://www.nsnam.org/wiki/index.php/PyViz