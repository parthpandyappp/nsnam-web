---
id: 1746
title: Google Summer of Code 2012 Wrap-up
date: 2012-09-03T08:01:14+00:00
author: lalith
layout: post
guid: http://www.nsnam.org/?p=1746
permalink: /news/google-summer-of-code-2012-wrap-up/
categories:
  - News
---
We’re happy to announce that all three of our Google Summer of Code projects have received passing evaluations. At the end of the summer, we asked mentors of each project to provide summaries of their respective students’ contributions over the summer, each of which is listed below.

<p style="text-align: center;">
  <img class="aligncenter" style="text-align: center;" src="http://upload.wikimedia.org/wikipedia/commons/5/5d/Ns-3-logo.png" alt="" width="275" height="50" />
</p>

<div>
  <p>
    Dizhi Zhou, mentored by Nicola Baldo and Marco Miozzo, developed a set of MAC schedulers to be used with the ns-3 LTE module. The ns-3 LTE module already supported a real-world API (the FemtoForum LTE MAC Scheduler Interface Specification) for the implementation of LTE MAC schedulers. However, at the time GSoC 2012 started, only a couple of scheduler implementations (Round Robin and Proportional Fair) were available; these implementations were very simple and were often found insufficient for running realistic LTE simulations with ns-3. To address this problem, Dizhi implemented several schedulers (FDMT, TDMT, TTA, FDBET, TDBET, FDTBFQ, TDTBFQ, PSS) based on their descriptions as published in the scientific literature. In addition to the implementation, Dizhi put a significant effort in writing documentation and validating the performance of the new schedulers using some known reference scenarios. The result is a set of well-documented and well-tested scheduler models that are expected to be merged with the main ns-3 code soon, and will be very useful for many ns-3 users. While carrying out his GSoC project, Dizhi became very experienced with the ns-3 LTE module, and also had the opportunity to interact with the community via mailing lists and code reviews. We hope that in the future Dizhi will stay with ns-3 as a valuable developer and community member. (Nicola Baldo)
  </p>
  
  <p>
    Sindhuja Venkatesh, mentored by Tom Henderson and Daniel Camara, developed IPv4 Netfilter and NAT code for ns-3 during her 2012 GSoC project. This project started by picking up netfilter and connection tracking code that had been partially completed during a previous GSoC project by Qasim Javed (mentored by Adrian Tam).  The first portion of the program worked on updating and refining the Netfilter portion of the code, which provides an extensible mechanism for inserting packet filtering and mangling operations into well-defined locations in the IPv4 stack.  As proof-of-concept, a basic NAT was written on top of this framework.  After the midterm exam, Sindhu continued to work on more complete NAT models by writing dedicated IPv4 NAT code hooked into the netfilter framework.  Two primary modes of NAT were implemented, a static one-to-one NAT and a dynamic NAT with port translation.  A NAT helper object was also written, along with a few examples and documentation for the models.  While there remain a few loose ends to complete before merging to ns-3-dev, we are hopeful to be able to complete and merge these models in the next ns-3 release cycle. (Tom Henderson)
  </p>
  
  <p>
    Mudit Gupta, mentored by Tommaso Pecorella, during his 2012 GSoC project designed and developed some interfaces aimed at integrating ns-3 with an HLA (High Level Architecture) Federation. This project started by choosing the reference HLA framework among the Open-Source available one. In particular <a href="http://www.porticoproject.org">Portico</a> and <a href="http://savannah.nongnu.org/projects/certi">CERTI</a> have been examined, choosing the first at the end. The first issue Mudit faced with was the way to communicate between the various federates. Although ns-3 is compliant with the latest LLVM compilers, the HLA components are not. Hence it was impossible to simply integrate the C++ HLA interfaces into ns-3. The chosen workaround was to build a Java ns-3 Federate object acting as a stub and communicating with the main ns-3 code through &#8220;normal&#8221; sockets. This solution is sub-optimal and the plan is to move to proper C++ interfeces as soon as the HLA implementations (i.e., Portico or CERTI) will be updated to support LLVM. The second problem was to synchronize the ns-3 time reference with the Federation one. The clocks might differ in time resolution and, ultimately, the ns-3 clock should advance only when the RTI clock allows it. The solution relies on a modification of the real-time ns-3 scheduler, with a virtual clock advanced by the RTI Federation messages rather than the hardware PC clock. The last issue Mudit worked on was the interaction between the Federates. The ns-3 module is able to receive Objects from the Federates along with their Attributes. Such Objects and Attributes at present are unspecified, as there is no actual ns-3 module using them. A proof-of-concept module has been developed, showing how to receive the Object and its Attributes. The HLA module is not yet fully completed, booth due to the Java wrapper (that should be considered as a temporary workaround) and the missing HLA-enabled ns-3 modules. The next steps to finalize it will be, beside the Java stub, to define new HLA-enabled ns-3 modules (or to modify some existing ones) along with its standard Objects and Attributes. The very last open point is to dynamically generate the ns-3 Federate description so to avoid to have to define it per-simulation. This could be accomplished through Python, however the topic has been left out by the GSOC task. After the final evaluation, also knowing the actual module&#8217;s limitation, Mudit committed himself to keep working on the module, so to remove some of them and make the module really usable and useful. (Tommaso Pecorella).
  </p>
  
  <p>
    We hope Dizhi, Sindhujha, and Mudit will continue their involvement with the ns-3 project, and community outside of GSoC as well. On a concluding note, we’d like to thank Google once again for accepting us into the programme this year. We look forward to applying for GSoC 2013!
  </p>
</div>