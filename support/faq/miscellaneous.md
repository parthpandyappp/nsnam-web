---
title: Miscellaneous
layout: page
permalink: /support/faq/miscellaneous/
---
<div id="toc_container" class="toc_wrap_right no_bullets">
  <ul class="toc_list">
    <li>
      <a href="#Putting_payload_in_a_Packet"><span class="toc_number toc_depth_1">1</span> Putting payload in a Packet</a>
    </li>
    <li>
      <a href="#Putting_real_bytes_in_the_payload"><span class="toc_number toc_depth_1">2</span> Putting real bytes in the payload</a>
    </li>
    <li>
      <a href="#Adding_data_to_an_existing_packet"><span class="toc_number toc_depth_1">3</span> Adding data to an existing packet</a>
    </li>
  </ul>
</div>

# <span id="Putting_payload_in_a_Packet">Putting payload in a Packet</span>

If you don't care about the actual content of the payload, the following will keep track of 1024 bytes of payload filled with zeros

<pre>uint32_t size = 1024;
Ptr&lt;Packet&gt; p = Create&lt;Packet&gt; (size);
</pre>

# <span id="Putting_real_bytes_in_the_payload">Putting real bytes in the payload</span>

If you do care about the actual content of the packet payload, you can also request the packet to copy the data in an internal buffer:

<pre>uint8_t *buffer = ...;
uint32_t size = ...;
Ptr&lt;Packet&gt; p = Create&lt;Packet&gt; (buffer, size);
</pre>

# <span id="Adding_data_to_an_existing_packet">Adding data to an existing packet</span>

If you have an existing packet and you need to add data at its start or its end, you need to create a new Header (Trailer). First, subclass the Header(Trailer) class:

<pre>class MyHeader : public Header
{
public:
  // new methods
  void SetData (uint16_t data);
  uint16_t GetData (void);
  // new method needed
  static TypeId GetTypeId (void);
  // overridden from Header
  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void Print (std::ostream &os) const;
private:
  uint16_t m_data;
};
</pre>

Typical implementations of the SetData/GetData methods will look like this:

<pre>void 
MyHeader::SetData (uint16_t data)
{
  m_data = data;
}
uint16_t
MyHeader::GetData (void)
{
  return m_data;
}
</pre>

While the overridden methods would look like this:

<pre>uint32_t 
MyHeader::GetSerializedSize (void) const
{
  // two bytes of data to store
  return 2;
}
void 
MyHeader::Serialize (Buffer::Iterator start) const
{
  start.WriteHtonU16 (m_data);
}
uint32_t 
MyHeader::Deserialize (Buffer::Iterator start)
{
  m_data = start.ReadNtohU16 ();
  return 2;
}
void 
MyHeader::Print (std::ostream &os) const
{
  os &lt;&lt; m_data;
}
</pre>

And, finally, you need to implement GetTypeId:

<pre>TypeId
MyHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MyHeader")
    .SetParent&lt;Header&gt; ()
    ;
  return tid;
}
</pre>

And, now, you can use this new header to store data in the packet as follows:

<pre>Ptr&lt;Packet&gt; p = ...;
MyHeader header = ...;
header.SetData (10);
p->AddHeader (header);
</pre>

or remove data from the packet:

<pre>Ptr&lt;Packet&gt; p = ...;
MyHeader header;
p->RemoveHeader (header);
header.GetData ();
</pre>
