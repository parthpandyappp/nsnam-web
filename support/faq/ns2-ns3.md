---
title: 'ns-2 and ns-3'
layout: page
permalink: /support/faq/ns2-ns3/
---
# The difference between ns-2 and ns-3

[ns-2](http://www.isi.edu/nsnam/ns) is a popular discrete-event network simulator developed under several previous research grants and activities. It remains in active use and will continue to be maintained.

ns-3 is a new software development effort focused on improving upon the core architecture, software integration, models, and educational components of ns-2. The project commenced in July 2006 and the first release was made on June 30, 2008.

# ns-2 scripts in ns-3

ns-2 scripts will not run within ns-3. ns-2 uses OTcl as its scripting environment. ns-3 uses C++ programs or python scripts to define simulations.

# ns-2 models in ns-3

Some ns-2 models that are mostly written in C++ have already been ported to ns-3: OLSR and Error Model were originally written for ns-2. OTcl-based models have not been and will not be ported since this would be equivalent to a complete rewrite.
