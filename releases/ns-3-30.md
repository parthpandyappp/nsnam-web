---
title: ns-3.30
layout: page
permalink: /releases/ns-3-30/
---
  <p>
    <b>Note:</b> Please see below about the ns-3.30.1 release update of ns-3.30.
  </p>

  <p>
    <b>ns-3.30</b> was released on August 21, 2019. ns-3.30 was updated to <b>ns-3.30.1</b> on September 18, 2019.  ns-3.30 includes the following new features:
  </p>

  <ul>
    <li>
      Radio Link Failure (RLF) functionality for LTE is now supported.
    </li>
    <li>
      Cobalt queue disc model for traffic control
    </li>
    <li>
      Wi-Fi preamble detection can now be modelled and is enabled by default.
    </li>
    <li>
      802.11ax spatial reuse features are now supported.
    </li>
    <li>
      LTE/EPC model enhanced with new features: SGW, PGW, MME are full nodes; new S5 interface between SGW and PGW, allows simulations with multiple SGW/PGW
    </li>
    <li>
      LTE eNB RRC extended to support improved S1 signalling model
    </li>
    <li>
      LTE backhaul links can now use any link technology, not just point-to-point
    </li>
    <li>
      ShowProgress object can be used to report on simulation execution
    </li>
  </ul>

  <p>
    Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.30/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
  </p>
  <p>
      The ns-3.30.1 release <b>download</b> is available from <a href="/releases/ns-allinone-3.30.1.tar.bz2">this link</a>.
  </p>

  <ul>
    <li>
      A patch to upgrade from ns-3.29 to ns-3.30 can be found <a href="/releases/patches/ns-3.29-to-ns-3.30.patch">here</a>
    </li>
    <li>
      A patch to upgrade from ns-3.30 to the updated ns-3.30.1 can be found <a href="/releases/patches/ns-3.30-to-ns-3.30.1.patch">here</a>
    </li>
    <li>
      What has changed since ns-3.29? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.30/CHANGES.html"> changes</a> and <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.30/RELEASE_NOTES"> RELEASE_NOTES</a> pages for ns-3.30. </li>
    <li>
      What has changed in the maintenance ns-3.30.1 updated release? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.30.1/RELEASE_NOTES">RELEASE_NOTES</a> page for ns-3.30.1. </li>
  </ul>
  <p>
    The <b>documentation</b> is available in several formats from <a href="/releases/ns-3-30/documentation">this link</a>.
  </p>
  <ul>
    <li>
      Errata containing late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
    </li>
  </ul>
