---
title: ns-3.24
layout: page
permalink: /releases/ns-3-24/
---
<div>
  <p>
    ns-3.24 was released on 15 September 2015. The main new model extension for this release is the addition of 802.11ac Very High Throughput (VHT) physical layer modes for Wi-Fi. This release also includes updates to the Waf build system and initial support for Python 3. The IPv4 ARP cache now supports the manual removal of entries and the addition of permanent entries. The SimpleChannel in the &#8216;network&#8217; module now allows per-NetDevice blacklists, in order to support hidden terminal testcases. Finally, the release includes about forty bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.24/file/dbd1a2b1c4ed/RELEASE_NOTES">RELEASE_NOTES</a>.
  </p>

  <p>
    A maintenance release, ns-3.24.1, was made on 30 September 2015 to fix three compilation and build system issues described in the <a href="https://www.nsnam.org/wiki/Ns-3.24-errata"> ns-3.24 errata</a>.
  </p>

  <ul>
    <li>
      The latest ns-3.24 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.24.1.tar.bz2">here</a>
    </li>
    <li>
      What has changed since ns-3.23? Consult the <a href="http://code.nsnam.org/ns-3.24/raw-file/dbd1a2b1c4ed/CHANGES.html">changes</a> page. <li>
        The documentation is available in several formats from <a href="/releases/ns-3-24/documentation">here</a>.
      </li>
      <li>
        Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
      </li>
      <li>
        A patch to upgrade from the last release (ns-3.23) to ns-3.24 can be found <a href="https://www.nsnam.org/release/patches/ns-3.23-to-ns-3.24.patch">here</a>
      </li>
      <li>
        A patch to upgrade from ns-3.24 to ns-3.24.1 can be found <a href="https://www.nsnam.org/release/patches/ns-3.24-to-ns-3.24.1.patch">here</a>
      </li>
