---
title: ns-3.27
layout: page
permalink: /releases/ns-3-27/
---
<div>
  <p>
    ns-3.27 was released on 12 October 2017 and features the following significant changes.
  </p>

  <ul>
    <li>
      <b>Carrier Aggregation</b> support was added to the LTE module.
    </li>
    <li>
      Native TCP models have been extended to include <b>SACK</b> and <b>LEDBAT</b> models.
    </li>
    <li>
      The Wi-Fi module was extended for partial <b>802.11ax High Efficiency (HE)</b> support, and a new <b>Robust Rate Adaptation Algorithm (RRPAA) rate control</b> has been added.
    </li>
    <li>
      The traffic-control module has been reworked, with new support for gathering <b>detailed statistics</b> about the operations of a queue disc, a new <b>multi-queue aware queue disc</b> modelled after the mq qdisc in Linux, and the ability to explicitly trace <b>queue sojourn time</b>.
    </li>
  </ul>

  <p>
    Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.27/raw-file/a6d4461e1a3d/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
  </p>

  <ul>
    <li>
      The latest ns-3.27 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.27.tar.bz2">here</a>
    </li>
    <li>
      What has changed since ns-3.26? Consult the <a href="http://code.nsnam.org/ns-3.27/raw-file/a6d4461e1a3d/CHANGES.html">changes</a> page. <li>
        The documentation is available in several formats from <a href="/releases/ns-3-27/documentation">here</a>.
      </li>
      <li>
        Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
      </li>
      <li>
        A patch to upgrade from the last release (ns-3.26) to ns-3.27 can be found <a href="https://www.nsnam.org/release/patches/ns-3.26-to-ns-3.27.patch">here</a>
      </li>
