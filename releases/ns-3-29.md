---
title: ns-3.29
layout: page
permalink: /releases/ns-3-29/
---
<div>
  <p>
    This ns-3 release is dedicated to the memory of project co-founder <a href="https://www.ece.gatech.edu/news/607255/loving-memory-george-riley">George Riley</a>.
  </p>

  <p>
    <b>ns-3.29</b> was released on September 4, 2018. ns-3.29 includes the following new features:
  </p>

  <ul>
    <li>
      An HTTP model based on a 3GPP reference model for HTTP/1.1
    </li>
    <li>
      A priority queue disc (PrioQueueDisc) for the traffic control module
    </li>
    <li>
      A model for the TCP Proportional Rate Reduction (PRR) recovery algorithm
    </li>
    <li>
      A node position allocator that rejects positions that are located within buildings defined in the scenario
    </li>
  </ul>

  <p>
    Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.29/file/078dcf663058/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.
  </p>

  <ul>
    <li>
      The latest ns-3.29 release source code can be downloaded from <a href="/releases/ns-allinone-3.29.tar.bz2">here</a>
    </li>
    <li>
      What has changed since ns-3.28? Consult the <a href="http://code.nsnam.org/ns-3.29/file/078dcf663058/CHANGES.html">changes</a> page. <li>
        The documentation is available in several formats from <a href="/releases/ns-3-29/documentation">here</a>.
      </li>
      <li>
        Errata containing late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
      </li>
      <li>
        A patch to upgrade from ns-3.28.1 to the updated ns-3.29 can be found <a href="/releases/patches/ns-3.28.1-to-ns-3.29.patch">here</a>
      </li>
