---
title: ns-3.23
layout: page
permalink: /releases/ns-3-23/
---
<div>
  <p>
    ns-3.23 was released on 14 May 2015. The main new features of this release are related to WiFi and Spectrum models. For Wi-Fi, support for two-level frame aggregation for IEEE 802.11n networks has been added, and several bugs and limitations in the 802.11n models have been fixed. Access points now establish BSSBasicRateSet for control frame transmissions, PLCP header and payload reception have been decoupled for improved PHY modeling accuracy, and RTS/CTS with A-MPDU is now fully supported. Several aspects of the mesh module were brought up from the pre-standard status to compliance with the 802.11s-2012 standard. In the spectrum module, a TvSpectrumTransmitter class has been added allowing the modeling of TV transmissions across a geographical region, to support wireless coexistence studies in the TV transmission bands. The model allows users to define transmit power spectral densities customized by attributes such as modulation type, power, antenna type, channel frequency, etc. Support for conversion from geographic to cartesian coordinates has been added to the mobility module. The <a href="http://code.nsnam.org/ns-3.23/file/0581642246f1/RELEASE_NOTES">RELEASE_NOTES</a> list the many bugs fixed and small improvements made.
  </p>

  <ul>
    <li>
      The latest ns-3.23 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.23.tar.bz2">here</a>
    </li>
    <li>
      What has changed since ns-3.22? Please also view the <a href="http://code.nsnam.org/ns-3.23/raw-file/0581642246f1/CHANGES.html">changes</a> page. <li>
        The documentation is available in several formats from <a href="/releases/ns-3-23/documentation">here</a>.
      </li>
      <li>
        Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
      </li>
      <li>
        A patch to upgrade from the last release (ns-3.22) can be found <a href="https://www.nsnam.org/release/patches/ns-3.22-to-ns-3.23.patch">here</a>
      </li>
